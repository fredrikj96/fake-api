# Task 3.12

### About this assignment
This is a Spring Boot application where we will add some structure and redefine the base project "Fake-api" we created a few days ago in Lesson 3.10.
The application uses controller methods reached with API platform programs such as Postman or Insomnia, to acquire and input data from a list of guitars.


In this task, the following was provided to the application:

*Changing data structure from List to Map<Integer, Guitar>.

*Using DI container to inject GuitarRespository dependency to controller.

*Returning ResponseEntity w. HTTP codes.

*Creating DTO which returns amount of guitars for each brand.
 
 
Base URL for this project is:
 
http://localhost:8080/api/v1/guitars

GET Request which returns a specific guitar by id:
 
http://localhost:8080/api/v1/guitars/id
 
GET Request which returns full list of guitars:
 
http://localhost:8080/api/v1/guitars, shows the current list of guitars.

GET Request which returns amount of guitars for each brand:

http://localhost:8080/api/v1/guitars/brands



### Other requests can be used to create, replace, modify or delete guitars in the list:

*POST Request on Base URL to create guitar.

*PUT Request on Base URL with added id as /{id}, to replace selected guitar.

*PATCH Request on Base URL with added id as /{id} to modify selected guitar.

*DELETE Request on Base URL with added id as /{id} to delete selected guitar.


 


