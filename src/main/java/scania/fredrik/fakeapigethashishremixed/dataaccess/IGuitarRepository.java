package scania.fredrik.fakeapigethashishremixed.dataaccess;

import scania.fredrik.fakeapigethashishremixed.models.domain.Guitar;
import scania.fredrik.fakeapigethashishremixed.models.dto.GuitarBrandDto;
import java.util.HashMap;


public interface IGuitarRepository {
    HashMap<Integer, Guitar> getAllguitars();
    HashMap<Integer, Guitar> getGuitar(int id);
    HashMap<Integer, Guitar> addGuitar(Guitar guitar);
    void replaceGuitar(Guitar guitar);
    void modifyGuitar(Guitar guitar);
        void deleteGuitar(int id);
    boolean guitarExists(int id);
    boolean isValidGuitar(Guitar guitar);
    GuitarBrandDto getGuitarBrandCounter();
}
