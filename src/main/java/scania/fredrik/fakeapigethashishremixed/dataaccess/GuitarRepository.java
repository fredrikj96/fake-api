package scania.fredrik.fakeapigethashishremixed.dataaccess;


import org.springframework.stereotype.Component;
import scania.fredrik.fakeapigethashishremixed.models.domain.Guitar;
import scania.fredrik.fakeapigethashishremixed.models.dto.GuitarBrandDto;
import scania.fredrik.fakeapigethashishremixed.models.maps.GuitarDtoMapper;

import java.util.HashMap;


@Component
public class GuitarRepository implements IGuitarRepository {
    private HashMap<Integer, Guitar> guitars = seedGuitars();

    private HashMap<Integer, Guitar> seedGuitars() {

        HashMap<Integer, Guitar> guitars = new HashMap<>();

        guitars.put(1, new Guitar(1, "Fender", "Telecaster"));
        guitars.put(2, new Guitar(2, "Jackson", "Kelly"));

        return guitars;
    }

    @Override
    public HashMap <Integer, Guitar> getAllguitars() {
        return guitars;
    }

    public HashMap <Integer, Guitar> getGuitar(int id) {


        return getGuitarHashMap(id);
    }


    @Override
    public HashMap<Integer, Guitar> addGuitar(Guitar guitar) {
        guitars.put(guitar.getId(), guitar);
        return getGuitarHashMap(guitar.getId());
    }

    @Override
    public void replaceGuitar(Guitar guitar) {
        var guitarToDelete = getGuitar(guitar.getId());
        guitars.remove(guitar.getId());
        addGuitar(guitar);

    }

    @Override
    public void modifyGuitar(Guitar guitar) {
        var guitarToModify = guitars.get(guitar.getId());
        guitarToModify.setModel(guitar.getModel());
        guitarToModify.setBrand(guitar.getBrand());

        getGuitarHashMap(guitar.getId());
    }

    @Override

    public void deleteGuitar(int id) {
        guitars.remove(id);
    }

    /*
    public void deleteGuitar(int id) {
        var guitarToDelete = getGuitar(id);
        guitars.remove(guitarToDelete);
     */

    @Override
    public boolean guitarExists(int id) {
        return getGuitar(id) != null;

    }

    private HashMap<Integer, Guitar> getGuitarHashMap(int id) {
        if (!guitars.containsKey(id)){
            return null;
        }
        HashMap<Integer, Guitar> guitarHashMap = new HashMap<>();
        guitarHashMap.put(id, guitars.get(id));
        return guitarHashMap;
    }


    public boolean isValidGuitar(Guitar guitar) {

         return guitar.getId() > 0 && guitar.getBrand() != null && guitar.getModel()  != null;

    }


    @Override
    public GuitarBrandDto getGuitarBrandCounter() {
        return GuitarDtoMapper.mapGuitarBrandsDto(guitars);
    }

    public boolean isValidGuitar(Guitar guitar, int id) {

        return isValidGuitar(guitar) && guitar.getId() == id;

    }


}
