package scania.fredrik.fakeapigethashishremixed.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import scania.fredrik.fakeapigethashishremixed.dataaccess.GuitarRepository;
import scania.fredrik.fakeapigethashishremixed.dataaccess.IGuitarRepository;
import scania.fredrik.fakeapigethashishremixed.models.domain.Guitar;
import scania.fredrik.fakeapigethashishremixed.models.dto.GuitarBrandDto;
import java.util.HashMap;


@RestController
@RequestMapping(value = "/api/v1/guitars")
public class GuitarController {

    @Autowired
    private IGuitarRepository guitarRepository = new GuitarRepository();
    @GetMapping
    public ResponseEntity<HashMap<Integer, Guitar>> getAllGuitars() {


        return new ResponseEntity<> (guitarRepository.getAllguitars(), HttpStatus.OK);
    }


    @GetMapping("/{id}")
    public ResponseEntity<HashMap<Integer, Guitar>> getGuitar(@PathVariable int id){
        if(!guitarRepository.guitarExists(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(guitarRepository.getGuitar(id), HttpStatus.OK);
    }

     @GetMapping("/brands")
     public ResponseEntity<GuitarBrandDto> brandList (@RequestBody Guitar guitar) {

        return new ResponseEntity<>(guitarRepository.getGuitarBrandCounter(), HttpStatus.OK);
     }


        @PostMapping
        public ResponseEntity<HashMap<Integer, Guitar>> createGuitar(@RequestBody Guitar guitar) {

        if (!guitarRepository.isValidGuitar(guitar)) {
            return new ResponseEntity<> (null, HttpStatus.BAD_REQUEST);
        }

        if (guitarRepository.guitarExists(guitar.getId())) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(guitarRepository.addGuitar(guitar), HttpStatus.CREATED);
        }

        @PutMapping("/{id}")

        public ResponseEntity<String> replaceGuitar(@PathVariable int id, @RequestBody Guitar guitar) {

            if (!guitarRepository.isValidGuitar(guitar)) {
                return new ResponseEntity<> ("The provided data is not valid", HttpStatus.BAD_REQUEST);
            }

            if (!guitarRepository.guitarExists(id)) {
                return new ResponseEntity<> ("This guitar does not exist, if you want it to be added, use a POST request", HttpStatus.NOT_FOUND);
            }


        guitarRepository.replaceGuitar(guitar);

        return new ResponseEntity<> ("Guitar has been replaced", HttpStatus.NO_CONTENT);

        }

        @PatchMapping("/{id}")
        public ResponseEntity<String> modifyGuitar(@PathVariable int id, @RequestBody Guitar guitar) {

            if (!guitarRepository.isValidGuitar(guitar))
                return new ResponseEntity<>("The provided data is not valid", HttpStatus.BAD_REQUEST);


            if (!guitarRepository.guitarExists(id)) {
                return new ResponseEntity<> ("This guitar does not exist, if you want it to be added, use a POST request", HttpStatus.NOT_FOUND);
            }


            guitarRepository.modifyGuitar(guitar);
            return new ResponseEntity<>("Guitar info has been updated/modified", HttpStatus.NO_CONTENT);
        }

        @DeleteMapping("/{id}")
        public ResponseEntity<String>  deleteGuitar (@PathVariable int id) {

            if (!guitarRepository.guitarExists(id)) {
                return new ResponseEntity<> ("This guitar does not exist", HttpStatus.NOT_FOUND);
            }
        guitarRepository.deleteGuitar(id);

        return new ResponseEntity<>("Guitar Deleted", HttpStatus.NO_CONTENT);

        }

}
