package scania.fredrik.fakeapigethashishremixed.models.dto;

import java.util.HashMap;

public class GuitarBrandDto {
    public GuitarBrandDto(HashMap<String, Integer> guitarBrandCounter) {
        this.guitarBrandCounter = guitarBrandCounter;
    }

    private HashMap<String, Integer> guitarBrandCounter;

    public HashMap<String, Integer> getGuitarBrandCounter() {
        return guitarBrandCounter;
    }

    public void setGuitarBrandCounter(HashMap<String, Integer> guitarBrandCounter) {
        this.guitarBrandCounter = guitarBrandCounter;
    }
}