package scania.fredrik.fakeapigethashishremixed.models.maps;
import scania.fredrik.fakeapigethashishremixed.models.domain.Guitar;
import scania.fredrik.fakeapigethashishremixed.models.dto.GuitarBrandDto;
import java.util.HashMap;


public class GuitarDtoMapper {


    private static int guitarBrandCounter(HashMap<Integer, Guitar> guitars, String brand){
        return (int) guitars
                .values()
                .stream()
                .filter(x -> x.getBrand().equals(brand))
                .count();
    }


        public static GuitarBrandDto mapGuitarBrandsDto(HashMap<Integer, Guitar> guitars) {
            HashMap<String, Integer> brandMap = new HashMap<>();
            for (Guitar guitar : guitars.values()) {
                String brand = guitar.getBrand();
                brandMap.put(brand, guitarBrandCounter(guitars, brand));
            }

            return new GuitarBrandDto(brandMap);

        }

    }
