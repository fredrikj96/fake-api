package scania.fredrik.fakeapigethashishremixed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FakeApiGetHashishRemixedApplication {

    public static void main(String[] args) {
        SpringApplication.run(FakeApiGetHashishRemixedApplication.class, args);
    }

}
